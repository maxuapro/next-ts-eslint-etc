import React from 'react'
import classes from './Navbar.module.css'
import Link from 'next/link'

function Navbar() {
    return (
        <div className={classes.navbar}>
            <h1>Navbar</h1>
            <Link href='/'>
                <a>
                    <h2>Home</h2>
                </a>
            </Link>

            <Link href='/info'>
                <a>
                    <h2>Info</h2>
                </a>
            </Link>
        </div>
    )
}

export default Navbar
